package com.cxt.movieapp.Model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hendri on 6/22/17.
 *
 */

public class Movie implements Parcelable {
    private int id;
    private int vote_count;
    private boolean video;
    private double vote_average;
    private String title;
    private double popularity;
    private String poster_path;
    private String original_language;
    private String original_title;
    private int[] genre_ids;
    private String backdrop_path;
    private boolean adult;
    private String overview;
    private String release_date;
    private Date release_date_dt;

    @SuppressWarnings("unused")
    public int getId() {
        return id;
    }

    @SuppressWarnings("unused")
    public void setId(int id) {
        this.id = id;
    }

    @SuppressWarnings("unused")
    public int getVote_count() {
        return vote_count;
    }

    @SuppressWarnings("unused")
    public void setVote_count(int vote_count) {
        this.vote_count = vote_count;
    }

    @SuppressWarnings("unused")
    public boolean isVideo() {
        return video;
    }

    @SuppressWarnings("unused")
    public void setVideo(boolean video) {
        this.video = video;
    }

    public double getVote_average() {
        return vote_average;
    }

    @SuppressWarnings("unused")
    public void setVote_average(double vote_average) {
        this.vote_average = vote_average;
    }

    public String getTitle() {
        return title;
    }

    @SuppressWarnings("unused")
    public void setTitle(String title) {
        this.title = title;
    }

    @SuppressWarnings("unused")
    public double getPopularity() {
        return popularity;
    }

    @SuppressWarnings("unused")
    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public String getPoster_path() {
        return poster_path;
    }

    private void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    @SuppressWarnings("unused")
    public String getOriginal_language() {
        return original_language;
    }

    @SuppressWarnings("unused")
    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    @SuppressWarnings("unused")
    public String getOriginal_title() {
        return original_title;
    }

    @SuppressWarnings("unused")
    public void setOriginal_title(String original_title) {
        this.original_title = original_title;
    }

    @SuppressWarnings("unused")
    public int[] getGenre_ids() {
        return genre_ids;
    }

    @SuppressWarnings("unused")
    public void setGenre_ids(int[] genre_ids) {
        this.genre_ids = genre_ids;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    @SuppressWarnings("unused")
    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    @SuppressWarnings("unused")
    public boolean isAdult() {
        return adult;
    }

    @SuppressWarnings("unused")
    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getOverview() {
        return overview;
    }

    @SuppressWarnings("unused")
    public void setOverview(String overview) {
        this.overview = overview;
    }

    @SuppressWarnings("unused")
    public String getRelease_date() {
        return release_date;
    }

    @SuppressWarnings("WeakerAccess")
    public void setRelease_date(String release_date) {
        this.release_date = release_date;
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.release_date_dt = sdf.parse(this.release_date);
        }
        catch (ParseException ex)
        {
            ex.printStackTrace();
        }
    }

    public Date getRelease_date_dt() {
        return release_date_dt;
    }

    @SuppressWarnings("unused")
    public void setRelease_date_dt(Date release_date_dt) {
        this.release_date_dt = release_date_dt;
    }

    @SuppressWarnings("unused")
    public Movie(){

    }

    @SuppressWarnings("WeakerAccess")
    public Movie(Parcel in){
        int length = in.readInt();
        this.id = in.readInt();
        this.vote_count = in.readInt();
        this.video = in.readInt() == 1;
        this.vote_average = in.readDouble();
        this.title = in.readString();
        this.popularity = in.readDouble();
        this.setPoster_path(in.readString());
        this.original_language = in.readString();
        this.original_title = in.readString();
        if(length > 0) {
            this.genre_ids = new int[length];
            in.readIntArray(this.genre_ids);
        }
        this.backdrop_path = in.readString();
        this.adult = in.readInt() == 1;
        this.overview = in.readString();
        this.setRelease_date(in.readString());
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        if(this.genre_ids != null)
            parcel.writeInt(this.genre_ids.length);
        else
            parcel.writeInt(0);

        parcel.writeInt(this.id);
        parcel.writeInt(this.vote_count);
        parcel.writeInt(this.video? 1:0);
        parcel.writeDouble(this.vote_average);
        parcel.writeString(this.title);
        parcel.writeDouble(this.popularity);
        parcel.writeString(this.poster_path);
        parcel.writeString(this.original_language);
        parcel.writeString(this.original_title);
        parcel.writeIntArray(this.genre_ids);
        parcel.writeString(this.backdrop_path);
        parcel.writeInt(this.adult? 1:0);
        parcel.writeString(this.overview);
        parcel.writeString(this.release_date);
    }

    public static final Parcelable.Creator CREATOR
            = new Parcelable.Creator() {

        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

}
