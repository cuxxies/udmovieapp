package com.cxt.movieapp.Utility;

import android.support.annotation.NonNull;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by hendri on 6/20/17.
 *
 */

public class APICallerListener implements Callback {
    private APICallback caller;

    public APICallerListener(APICallback caller) {
        this.caller = caller;
    }

    @Override
    public void onFailure(@NonNull Call call, @NonNull IOException e) {
        e.printStackTrace();
        if(caller != null)
           caller.errorOnAPICall(e);
        //caller = null;
    }

    @Override
    public void onResponse(@NonNull Call call, @NonNull Response response) {
        JSONObject jsonObject;
        try {
            //noinspection ConstantConditions
            jsonObject = new JSONObject(response.body().string());
            if(caller != null)
                caller.didCompleteAPICallWithJSON(jsonObject);
            //caller = null;
        }
        catch (NullPointerException ex)
        {
            ex.printStackTrace();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            if(caller != null)
                caller.errorOnAPICall(ex);
            //caller = null;
        }
    }


    public void cancelCurrentAPICall()
    {
        Integer tag = this.caller.hashCode();
        APICaller.getInstance().cancelAPICall(tag);
        this.caller = null;
    }

    public interface APICallback{
        void didCompleteAPICallWithJSON(JSONObject jsonObject);
        void errorOnAPICall(Exception ex);
    }


}
