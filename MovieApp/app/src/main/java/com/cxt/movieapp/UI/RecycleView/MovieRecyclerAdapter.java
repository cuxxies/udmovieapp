package com.cxt.movieapp.UI.RecycleView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cxt.movieapp.Model.Movie;
import com.cxt.movieapp.R;

import com.cxt.movieapp.Utility.APICaller;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MovieRecyclerAdapter extends RecyclerView.Adapter<MovieRecyclerAdapter.MovieViewHolder> {

    private final ArrayList<Movie> movieList;
    private final MovieItemClickListener mCaller;
    static class MovieViewHolder extends RecyclerView.ViewHolder {
        private final View view;
        private final TextView mTitle ;
        private final ImageView mPoster;
        private Movie mMovie;
        MovieViewHolder(View v) {
            super(v);
            view = v;
            mTitle= (TextView)view.findViewById(R.id.recycleview_item_movie_title);
            mPoster = (ImageView)view.findViewById(R.id.recycleview_item_movie_image);
        }

        void bindMovie(Movie movie)
        {
            mMovie = movie;
            Picasso.with(mPoster.getContext()).load(APICaller.getImageURL(movie.getPoster_path(), APICaller.IMAGE_SIZE.THUMBNAILS)).fit().into(mPoster);
            mTitle.setText(mMovie.getTitle());
        }



    }

    public MovieRecyclerAdapter(ArrayList<Movie> movieList, MovieItemClickListener caller) {
        this.movieList = movieList;
        this.mCaller = caller;
    }

    public void appendMovie(ArrayList<Movie> movieList)
    {
//        int index = 0;
//        for(Movie movie: movieList)
//        {
//            this.movieList.add(movie);
//        }
        this.movieList.addAll(movieList);
    }

//    public ArrayList<Movie> getMovieList() {
//        return movieList;
//    }
//
//    public void setMovieList(ArrayList<Movie> movieList) {
//        this.movieList = movieList;
//    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_movie_itemview, parent, false);
        return new MovieViewHolder(v);

    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, final int position) {
        final int index = holder.getAdapterPosition();
        holder.bindMovie(movieList.get(position));
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCaller.movieItemTouchedAt(index);
            }
        });
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }


    public void clearAllMovie()
    {
        movieList.clear();
    }

    public Movie getMovieAtIndex(int index)
    {
        return movieList.get(index);
    }

    public interface MovieItemClickListener{
        void movieItemTouchedAt(int index);
    }
}
