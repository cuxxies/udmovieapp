package com.cxt.movieapp.UI.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cxt.movieapp.Model.Movie;
import com.cxt.movieapp.R;
import com.cxt.movieapp.UI.RecycleView.EndlessScrollListener;
import com.cxt.movieapp.UI.RecycleView.MovieRecyclerAdapter;
import com.cxt.movieapp.Utility.APICaller;
import com.cxt.movieapp.Utility.APICallerListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieListActivity extends AppCompatActivity implements APICallerListener.APICallback, MovieRecyclerAdapter.MovieItemClickListener {
    @SuppressWarnings({"WeakerAccess", "CanBeFinal"})
    @BindView(R.id.movielist_loading_data_dialog) LinearLayout mLoadingDataDialog;
    @SuppressWarnings({"WeakerAccess", "CanBeFinal"})
    @BindView(R.id.movielist_recycleview) RecyclerView mRecyclerView;
    private MovieRecyclerAdapter mAdapter;
    @SuppressWarnings("FieldCanBeLocal")
    private GridLayoutManager mLayoutManager;
    private APICallerListener apiCallerListener;
    @SuppressWarnings("FieldCanBeLocal")
    private EndlessScrollListener scrollListener;
    private String sort_mode = "";
    private int currentPage = 1;
    private boolean goToSetting = false;
    @Override
    protected void onResume() {
        super.onResume();
        if(goToSetting)
        {
            goToSetting = false;
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            String newSortMode = prefs.getString("sort_mode", "popular");
            if(!newSortMode.equals(sort_mode))
            {
                sort_mode = newSortMode;
                mAdapter.clearAllMovie();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });
                callMovieAPI(null);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.movie_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                goToSetting = true;
                Intent i = new Intent(MovieListActivity.this, SettingsActivity.class);
                startActivity(i);
                break;
            case R.id.refresh:
                reloadWholeData();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);
        ButterKnife.bind(this);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        sort_mode = prefs.getString("sort_mode", "popular");



        int widthSize = 2;
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            widthSize = 4;

        mLayoutManager = new GridLayoutManager(this,widthSize);
        mAdapter = new MovieRecyclerAdapter(new ArrayList<Movie>(), this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        apiCallerListener = new APICallerListener(this);
        callMovieAPI(null);

        scrollListener = new EndlessScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextPageData();
            }
        };
        mRecyclerView.addOnScrollListener(scrollListener);
    }

    private void loadNextPageData()
    {
            currentPage++;
            HashMap<String, String> param = new HashMap<>();
            param.put("page", String.valueOf(currentPage));
            callMovieAPI(param);
    }

    @Override
    public void didCompleteAPICallWithJSON(JSONObject jsonObject) {
        try {
            Log.v("CallResult",jsonObject.toString());
            JSONArray results = jsonObject.getJSONArray("results");
            ArrayList<Movie> movieArray = APICaller.convertJsonMovieArrayToPOJO(results);
            mAdapter.appendMovie(movieArray);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAdapter.notifyItemInserted(mAdapter.getItemCount() - 1);
                    hideLoadingDialog();
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void errorOnAPICall(Exception ex) {
        ex.printStackTrace();
        runOnUiThread(new Runnable() {
            public void run() {
                CharSequence text = getString(R.string.fetching_data_failed);
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(getApplicationContext(), text, duration);
                toast.show();
                hideLoadingDialog();
            }
        });

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                scrollListener.resetLoading();
            }
        }, 5000);
    }

    @Override
    protected void onDestroy() {
        apiCallerListener.cancelCurrentAPICall();
        apiCallerListener = null;
        super.onDestroy();
    }

    @Override
    public void movieItemTouchedAt(int index) {
        Movie selectedMovie =  mAdapter.getMovieAtIndex(index);
        Intent intent = new Intent(this, MovieDetailActivity.class);
        intent.putExtra("movie",selectedMovie);
        startActivity(intent);
    }

    private void showLoadingDialog()
    {
        mLoadingDataDialog.setVisibility(View.VISIBLE);
    }

    private void hideLoadingDialog()
    {
        mLoadingDataDialog.setVisibility(View.INVISIBLE);
    }

    private void callMovieAPI(@Nullable HashMap<String,String> param)
    {
        showLoadingDialog();
        APICaller.getInstance().callURLGetMovieList(param,apiCallerListener,true,this,sort_mode);
    }

    private void reloadWholeData()
    {
        int itemCount = mAdapter.getItemCount();
        mAdapter.clearAllMovie();
        mAdapter.notifyItemRangeRemoved(0,itemCount);
        callMovieAPI(null);
    }
}
