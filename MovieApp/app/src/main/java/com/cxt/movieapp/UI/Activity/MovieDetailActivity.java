package com.cxt.movieapp.UI.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.cxt.movieapp.Model.Movie;
import com.cxt.movieapp.R;
import com.cxt.movieapp.Utility.APICaller;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieDetailActivity extends AppCompatActivity {
    @SuppressWarnings({"CanBeFinal", "unused"})
    @BindView(R.id.detailmovie_movie_backdrop) ImageView mBackdrop;
    @SuppressWarnings({"CanBeFinal", "unused"})
    @BindView(R.id.detailmovie_movie_poster) ImageView mPoster;
    @SuppressWarnings({"CanBeFinal", "unused"})
    @BindView(R.id.detailmovie_movie_overview) TextView mOverview;
    @SuppressWarnings({"CanBeFinal", "unused"})
    @BindView(R.id.detailmovie_movie_releasedate) TextView mReleaseDate;
    @SuppressWarnings({"CanBeFinal", "unused"})
    @BindView(R.id.detailmovie_movie_title) TextView mTitle;
    @SuppressWarnings({"CanBeFinal", "unused"})
    @BindView(R.id.detailmovie_movie_userrating) TextView mUserRating;
    @SuppressWarnings("FieldCanBeLocal")
    private Movie movie;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        if(intent.hasExtra("movie")) {
            movie = intent.getParcelableExtra("movie");
            Picasso.with(getApplicationContext()).load(APICaller.getImageURL(movie.getPoster_path(), APICaller.IMAGE_SIZE.EXTRA_LARGE)).into(mPoster);
            Picasso.with(getApplicationContext()).load(APICaller.getImageURL(movie.getBackdrop_path(), APICaller.IMAGE_SIZE.HD_READY)).into(mBackdrop);
            mTitle.setText(movie.getTitle());
            mOverview.setText(movie.getOverview());
            @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
            String releaseDateStr = simpleDateFormat.format(movie.getRelease_date_dt());
            mUserRating.setText(String.format(getString(R.string.rating_placeholder),String.valueOf(movie.getVote_average())));
            mReleaseDate.setText(String.format(getString(R.string.release_date_placeholder),releaseDateStr));
        }
    }
}
