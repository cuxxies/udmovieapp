package com.cxt.movieapp.Utility;

import com.cxt.movieapp.Model.Movie;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.CacheControl;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by hendri on 6/20/17.
 *
 */



public class APICaller {
    public enum SELECTED_API {
        POPULAR,
        TOP_RATED
    }

    public enum IMAGE_SIZE{
        THUMBNAILS,
        MEDIUM,
        LARGE,
        EXTRA_LARGE,
        HD_READY,
        FULLHD
    }

    private final OkHttpClient okHttpClient;
    private static APICaller self;

    private static final String BASE_URL = "https://api.themoviedb.org/3";
    private static final String API_KEY = "b6ca19f668d748745e31f3f98e7fcefc";
    private static final String POPULAR_PATH = "/movie/popular";
    private static final String TOP_PATH = "/movie/top_rated";

    private APICaller() {
        okHttpClient = new OkHttpClient();
    }

    public static APICaller getInstance()
    {
        if(self == null){
            self = new APICaller();
        }
        return self;
    }
    public static HashMap<String,String> getDefaultParameters()
    {
        HashMap<String,String> parameters = new HashMap<>();
        parameters.put("api_key",API_KEY);
        return parameters;
    }
    private static HashMap<String,String> putAllRetainOldValue(HashMap<String, String> origin, HashMap<String, String> addition)
    {
        for(String s: addition.keySet())
        {
            origin.put(s,addition.get(s));
        }
        return origin;
    }
    public static String mapParametersToURLStringParams(HashMap<String,String> param)
    {
        String strResult = "";
        int index = 0;
        for(String key : param.keySet())
        {
            try {
                if(index == 0)
                {
                    strResult = "?"+key+"="+URLEncoder.encode(param.get(key),"UTF-8");
                }
                else
                    strResult = strResult+"&"+ key+"="+URLEncoder.encode(param.get(key),"UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            index++;
        }
        return strResult;
    }

    public static String buildURLWithParameters(HashMap<String,String> param, SELECTED_API selected_api)
    {
        String path = "";
        switch (selected_api){
            case POPULAR:
                path = POPULAR_PATH;
                break;
            case TOP_RATED:
                path = TOP_PATH;
                break;
        }

        HashMap<String,String> parameter = new HashMap<>();
        parameter.putAll(getDefaultParameters());
        if(param != null){
           parameter = APICaller.putAllRetainOldValue(parameter,param);
            //parameter.putAll(param);
        }
        return BASE_URL+path+APICaller.mapParametersToURLStringParams(parameter);
    }

    private void callURLGet(String url, APICallerListener callerHandler, boolean isAsync, Object caller)
    {
        Request request = new Request.Builder().cacheControl(new CacheControl.Builder()
                .maxStale(2, TimeUnit.HOURS)
                .build())
                .url(url).tag(caller.hashCode())
                .build();
        Call call = okHttpClient.newCall(request);
        try {
            Response response;
            if(isAsync) {
                call.enqueue(callerHandler);
            }
            else{
                response = call.execute();
                callerHandler.onResponse(call,response);
            }
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            callerHandler.onFailure(call, ex);
        }
    }

    //TODO finish method getURLGetMovieList
    public void callURLGetMovieList(HashMap<String,String> param, APICallerListener callerListener, @SuppressWarnings("SameParameterValue") boolean isAsync, Object caller, String sortMode)
    {
        if(sortMode.equals("popular"))
            callURLGet(APICaller.buildURLWithParameters(param,SELECTED_API.POPULAR),callerListener,isAsync, caller);
        else
            callURLGet(APICaller.buildURLWithParameters(param,SELECTED_API.TOP_RATED),callerListener,isAsync, caller);
    }

    public void cancelAPICall(Integer tag)
    {
        for(Call call : okHttpClient.dispatcher().queuedCalls()) {
            if(call.request().tag().equals(tag))
                call.cancel();
        }
        for(Call call : okHttpClient.dispatcher().runningCalls()) {
            if(call.request().tag().equals(tag))
                call.cancel();
        }
    }

    public static ArrayList<Movie> convertJsonMovieArrayToPOJO(JSONArray moviesArray)
    {
        ObjectMapper mapper = new ObjectMapper();
        ArrayList<Movie> movieList = new ArrayList<>();
        try {
            movieList = mapper.readValue(moviesArray.toString(),new TypeReference<ArrayList<Movie>>(){});
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        return movieList;
    }

    public static String getImageURL(String string, IMAGE_SIZE image_size)
    {
        String size = "";
        switch (image_size)
        {
            case THUMBNAILS:
                size = "w185";
                break;
            case MEDIUM:
                size = "w342";
                break;
            case LARGE:
                size = "w500";
                break;
            case EXTRA_LARGE:
                size = "w780";
                break;
            case HD_READY:
                size = "w1280";
                break;
            case FULLHD:
                size = "w1920";
                break;
        }
        return "http://image.tmdb.org/t/p/"+size+"//"+string;
    }
}
