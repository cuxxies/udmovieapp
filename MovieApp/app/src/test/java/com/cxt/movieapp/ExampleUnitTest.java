package com.cxt.movieapp;

import com.cxt.movieapp.Utility.APICaller;
import com.cxt.movieapp.Utility.APICallerListener;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest implements APICallerListener.APICallback {
    @Test
    public void testURLBuilder() throws Exception {
        HashMap<String,String> map =  APICaller.getDefaultParameters();
        Assert.assertTrue(map.get("api_key") != null);
        map.put("doremikey","doremivalue");
        Assert.assertEquals(APICaller.mapParametersToURLStringParams(map),"?api_key=b6ca19f668d748745e31f3f98e7fcefc&"+"doremikey=doremivalue");

        map.put("unsafe","ABCD EF / AB \\ AND &");
        Assert.assertEquals(APICaller.mapParametersToURLStringParams(map),"?api_key=b6ca19f668d748745e31f3f98e7fcefc&doremikey=doremivalue&unsafe=ABCD+EF+%2F+AB+%5C+AND+%26");

        map.clear();
        String actualURL = "https://api.themoviedb.org/3"+"/movie/popular"+"?api_key=b6ca19f668d748745e31f3f98e7fcefc";
        Assert.assertEquals(APICaller.buildURLWithParameters(map,APICaller.SELECTED_API.POPULAR),actualURL);

        actualURL = "https://api.themoviedb.org/3"+"/movie/top_rated"+"?api_key=b6ca19f668d748745e31f3f98e7fcefc";
        Assert.assertEquals(APICaller.buildURLWithParameters(map,APICaller.SELECTED_API.TOP_RATED),actualURL);
    }

    @Override
    public void didCompleteAPICallWithJSON(JSONObject jsonObject) {

    }

    @Override
    public void errorOnAPICall(Exception ex) {

    }
}