package com.cxt.movieapp;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.cxt.movieapp.Model.Movie;
import com.cxt.movieapp.Utility.APICaller;
import com.cxt.movieapp.Utility.APICallerListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest implements APICallerListener.APICallback  {
    private CountDownLatch signal = null;

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.cxt.movieapp", appContext.getPackageName());
    }

    //TODO create test for URL Calling and assert all the json result
    @Test
    public void testCallURL()
    {
        signal = new CountDownLatch(1);
        APICallerListener apiCallerListener = new APICallerListener(this);
        APICaller.getInstance().callURLGetMovieList(null,apiCallerListener,true, this,"popular");
        try {
            signal.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //region Callback Result from API Caller
    @Override
    public void didCompleteAPICallWithJSON(JSONObject jsonObject) {
        try {
            JSONArray results = jsonObject.getJSONArray("results");
            ArrayList<Movie> movieArray = APICaller.convertJsonMovieArrayToPOJO(results);
            Assert.assertEquals(movieArray.size(),20);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        signal.countDown();
    }

    @Override
    public void errorOnAPICall(Exception ex) {
        ex.printStackTrace();
        signal.countDown();
    }
    //endregion
}
